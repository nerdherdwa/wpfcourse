﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Course.Classes;

namespace WPF_Course
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //GetToDoItems();

        }

        private void GetToDoItems()
        {
            //List<ToDoItem> todoItems = new List<ToDoItem>();
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 20, ItemName = "add code to base class" });
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 0, ItemName = "design template" });
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 50, ItemName = "fix the leaking drain" });
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 73, ItemName = "make bed" });
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 100, ItemName = "take a nap" });
            //todoItems.Add(new ToDoItem() { ItemCompletionPercentage = 12, ItemName = "eat lunch" });

            //itemsControlTodo.ItemsSource = todoItems;

        }

        private void btnSayHello_Click(object sender, RoutedEventArgs e)
        {
            string userName = btnBoxUserName.Text;
            string firstName = btnBoxFirstName.Text;
            string lastName = btnBoxLastName.Text;

            int i;
            string s = btnBoxAge.Text;
            if (Int32.TryParse(s, out i))
            {
                i = Int32.Parse(s);
            }
            else
            {
                i = 0;
            }

            Person person = new Person()
            {
                Age = i,
                LastName = lastName,
                FirstName = firstName
            };

            string message = string.Format("Username = {0}{4}Name = {1}, {2}{4}Age = {3}", userName, person.LastName, person.FirstName, person.Age, Environment.NewLine);
            MessageBox.Show(message);
        }
    }

    public class ToDoItem
    {
        public string ItemName { get; set;}
        public int ItemCompletionPercentage { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}% completed", ItemName, ItemCompletionPercentage);
        }
    }

}
