﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Interface_Sample.Classes;

namespace WPF_Interface_Sample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Person person;
        People people;
        public MainWindow()
        {
            InitializeComponent();
            person = new Person();
            stackPanelPerson.DataContext = person;

            //people = new Classes.People();
            //listPeople.DataContext = people;

        }

        private void btnMessage_Click(object sender, RoutedEventArgs e)
        {
           // personControl.Person = person;
        }
    }
}
