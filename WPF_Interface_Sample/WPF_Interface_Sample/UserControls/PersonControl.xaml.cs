﻿using System;
using System.Windows;
using System.Windows.Controls;
using WPF_Interface_Sample.Classes;

namespace WPF_Interface_Sample.UserControls
{
    /// <summary>
    /// Interaction logic for PersonControl.xaml
    /// </summary>
    public partial class PersonControl : UserControl
    {
        //private Person person;

        //public Person Person
        //{
        //    get { return person; }
        //    set
        //    {
        //        person = value;
        //        MainContainer.DataContext = person;
        //    }
        //}


        public Person Person
        {
            get { return (Person)GetValue(PersonProperty); }
            set { SetValue(PersonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Person.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PersonProperty =
            DependencyProperty.Register("Person", typeof(Person), typeof(PersonControl), new PropertyMetadata(new Person(), GetValue));

        private static void GetValue(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            PersonControl personControl = sender as PersonControl;
            if (personControl != null)
            {
                personControl.MainContainer.DataContext = args.NewValue as Person;
            }
        }

        public PersonControl()
        {
            InitializeComponent();
        }
    }
}
