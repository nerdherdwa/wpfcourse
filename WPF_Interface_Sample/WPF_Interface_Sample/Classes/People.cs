﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Interface_Sample.Classes
{
    public class People : ObservableCollection<Person>
    {
        public People()
        {
            for (int i = 0; i < 10; i++)
            {
                Random r = new Random();
                int randomNo = r.Next(0, 100);

                Add(new Person()
                {
                    FirstName = "Name" + i,
                    LastName = "LName" + (i * 2),
                    Age = randomNo
                });
            }
        }
    }
}
